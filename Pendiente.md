# Cuestiones a pensar:

- Es necesario definir la extencibilidad de cada módulo para poder compartir estructuras lógicas en común que mantengan la concepción de unión interpretada,
en un contexto determinado, por ejemplo:


```
si podemos definir e interpretar un esquema de datos  a travez de cualquier
punto de enfoque, podemos abstraer
la función especifica del modulo, de sus partes que lo integran
que permita utilizar metodos que seran evaluados y que sus respuestas
dependeran del contexto en que es ejecutada

```

## Planteo de Escenario1 - Importar excel

Para poder leer el contenido del excel, es necesario enviar desde el
```CLIENTE```, una ```PROMESA```
(
    [Pomise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)
)


```
Cliclo de promesa basica
------------------------

                    [CATCH]
                       |
[CLIENTE]          [PROMISE]---[success|error|...]
  |                    |
  +--[Send to]--┐    [API]
                |      |
                |      |
                +---[ROUTE]
                |      |
            [REQUEST]--+
```

