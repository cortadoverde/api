# Configuracion
Instalar nginx para crear un proxy y tener el mismo origen de request para la API y la App web.

Es necesario correr dos servicios, uno el encargado de la API, y el otro el encargado de la aplicacion web.



**/etc/nginx/sites-available/tabaWebApp**

```
server {
        listen 81 default_server;
        listen [::]:81 default_server ipv6only=on;

        server_name api.taba;

        location / {
            proxy_pass http://192.168.0.105:8060;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host $host;
            proxy_cache_bypass $http_upgrade;
        }

        location /api {
            proxy_pass http://192.168.0.105:8080;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host $host;
            proxy_cache_bypass $http_upgrade;
        }
}
```

## Deamon - Iniciador de servicio API

**/etc/init/taba-api.conf**

```
description     "Taba Web Service 1.0"
author          "Taba Software - info@tabasoftware.com.ar"

start on networking
stop on shutdown

# This line is needed so that Upstart reports the pid of the Node.js process
# started by Forever rather than Forever's pid.
expect fork

# The following environment variables must be set so as to define where Node.js
# and Forever binaries and the Node.js source code can be found.
#
# The example environment variables below assume that Node.js is installed by
# building from source with the standard settings.
#
# It should be easy enough to adapt to the paths to be appropriate to a package
# installation, but note that the packages available in the default repositories
# are far behind the times. Most users will be  building from source to get a
# more recent Node.js version.
#
# The full path to the directory containing the node and forever binaries.
# env NODE_BIN_DIR="/usr/local/bin"
# Set the NODE_PATH to the Node.js main node_modules directory.
# env NODE_PATH="/usr/local/lib/node_modules"
# The application startup Javascript file path.
# env APPLICATION_PATH="/home/node/my-application/start-my-application.js"
# Process ID file path.
# env PIDFILE="/var/run/my-application.pid"
# Log file path.
# env LOG="/var/log/my-application.log"
# Forever settings to prevent the application spinning if it fails on launch.
# env MIN_UPTIME="5000"
# env SPIN_SLEEP_TIME="2000"

env NODE_BIN_DIR="/usr/local/bin"
env NODE_PATH="/usr/local/lib/node_modules"
env APPLICATION_PATH="/srv/apps/api/app.js"
env PIDFILE="/var/run/taba-api.pid"
env LOG="/var/log/taba-api.log"
env MIN_UPTIME="5000"
env SPIN_SLEEP_TIME="2000"

script
    # Add the node executables to the path, which includes Forever if it is
    # installed globally, which it should be.
    PATH=$NODE_BIN_DIR:$PATH
    # The minUptime and spinSleepTime settings stop Forever from thrashing if
    # the application fails immediately on launch. This is generally necessary
    # to avoid loading development servers to the point of failure every time
    # someone makes an error in application initialization code, or bringing
    # down production servers the same way if a database or other critical
    # service suddenly becomes inaccessible.
    exec forever \
      --pidFile $PIDFILE \
      --spinSleepTime $SPIN_SLEEP_TIME \
      start $APPLICATION_PATH
end script

pre-stop script
    # Add the node executables to the path.
    PATH=$NODE_BIN_DIR:$PATH
    # Here we're using the pre-stop script to stop the Node.js application
    # process so that Forever is given a chance to do its thing and tidy up
    # its data. Note that doing it this way means that each application that
    # runs under Forever must have a different start file name, regardless of
    # which directory it is in.
    exec forever stop $APPLICATION_PATH
end script
```

**Servidor web**
[repositorio](https://bitbucket.org/tabasoftware/taba-webserver)

## Deamon - Iniciador de servicio Servidor web

**/etc/init/taba-srv.conf**

```
description     "Taba Server Web 1.0"
author          "Taba Software - info@tabasoftware.com.ar"

start on startup
stop on shutdown

# This line is needed so that Upstart reports the pid of the Node.js process
# started by Forever rather than Forever's pid.
expect fork

# The following environment variables must be set so as to define where Node.js
# and Forever binaries and the Node.js source code can be found.
#
# The example environment variables below assume that Node.js is installed by
# building from source with the standard settings.
#
# It should be easy enough to adapt to the paths to be appropriate to a package
# installation, but note that the packages available in the default repositories
# are far behind the times. Most users will be  building from source to get a
# more recent Node.js version.
#
# The full path to the directory containing the node and forever binaries.
# env NODE_BIN_DIR="/usr/local/bin"
# Set the NODE_PATH to the Node.js main node_modules directory.
# env NODE_PATH="/usr/local/lib/node_modules"
# The application startup Javascript file path.


env NODE_BIN_DIR="/usr/local/bin"
env NODE_PATH="/usr/local/lib/node_modules"
env APPLICATION_PATH="/srv/apps/taba-webserver/server.js"
env PIDFILE="/var/run/taba-srv.pid"
env LOG="/var/log/taba-srv.log"
env MIN_UPTIME="5000"
env SPIN_SLEEP_TIME="2000"

script
    # Add the node executables to the path, which includes Forever if it is
    # installed globally, which it should be.
    PATH=$NODE_BIN_DIR:$PATH
    # The minUptime and spinSleepTime settings stop Forever from thrashing if
    # the application fails immediately on launch. This is generally necessary
    # to avoid loading development servers to the point of failure every time
    # someone makes an error in application initialization code, or bringing
    # down production servers the same way if a database or other critical
    # service suddenly becomes inaccessible.
    exec forever \
      --pidFile $PIDFILE \
      -a \
      -l $LOG \
      --minUptime $MIN_UPTIME \
      --spinSleepTime $SPIN_SLEEP_TIME \
      start $APPLICATION_PATH
end script

pre-stop script
  # Add the node executables to the path.
  PATH=$NODE_BIN_DIR:$PATH
  # Here we're using the pre-stop script to stop the Node.js application
  # process so that Forever is given a chance to do its thing and tidy up
  # its data. Note that doing it this way means that each application that
  # runs under Forever must have a different start file name, regardless of
  # which directory it is in.
  exec forever stop $APPLICATION_PATH
end script

```


# Modelos
```
model
├── model/comprobante.js
├── model/config.js
├── model/cuenta.js
├── model/descargas.js
├── model/empresa.js
├── model/notadeventa.js
├── model/pedidos.js
├── model/productos.js
├── model/stock.js
└── model/usuario.js
```

## comprobante
```
  url: /api/comprobante/:tipoId/:numeroId
```  

**getComprobante** ( tipoId, numeroId, callback )

**Respuesta**
```
{
  "NombreImprimir": "Factura                  ",
  "Cuenta": "0003  ",
  "Comprobante": "A000100001704",
  "FechaSql": "2015-07-01T00:00:00.000Z",
  "RazonSocial": "Craze S.A.                         ",
  "Domicilio": "Av Pte Peron 969              ",
  "CodigoPostal": "5900        ",
  "ImporteSubtotalPanta": 2689.18,
  "ImporteDescuento": 0,
  "ImporteIVA": 564.73,
  "ImporteCbte": 3253.91,
  "BonificacionAcordada": "               ",
  "BonificacionProntoPago": "               ",
  "movimientos": [
    {
      "CodProducto": "M934-20x11R9   ",
      "Cantidad": 2,
      "Nombre": "RAZR2 M934 AT 20 x 11 R9 Tra       ",
      "ImporteFac": 2689.18
    }
  ]
}
```
