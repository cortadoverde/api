var cfgApp = require('./cfg/app');

cfgApp._root = __dirname;

GLOBAL.TabaApp = cfgApp;

if (!Date.now) {
    Date.now = function() { return new Date().getTime(); }
}

var
      port = process.env.PORT || 8080,
  express  = require('express'),
      app  = express(),
      fs   = require('fs'),
bodyParser = require('body-parser'),
      auth = require('./lib/auth')
    logger = require('morgan'),
 multipart = require('connect-multiparty'),
    xlsx   = require('xlsx'),
         _ = require('lodash'),
    multer = require('multer'),
 multipartMiddleware = multipart()
;

var upload = multer({ dest : __dirname + '/files/'});


// Rutas
var routes = {};
    routes.usuarios     = require ('./route/usuarios');
    routes.productos    = require ('./route/productos');
    routes.empresa      = require ('./route/empresa');
    routes.pedidos      = require ('./route/pedidos');
    routes.stock        = require ('./route/stock');
    routes.cuenta       = require ('./route/cuenta');
    routes.descargas    = require ('./route/descargas');
    routes.comprobante  = require ('./route/comprobante');

// Configuracion del body Parser
app.use( bodyParser.urlencoded({ extended: true}) );
app.use( bodyParser.json() );
app.use(logger('dev'));


// Iniciar servidor
var server = app.listen(port);
server.timeout = ( 2 * 60 * 1000);
// Metodos de seguridad



app.get('/api/test', function (req, res){
  var m = require('./model/stock');
  m.findByCodigo('FA101');
  return res.send('data')
})
// Definicion de rutas
app.post('/api/user/login', routes.usuarios.login);
app.get('/api/user/logout', routes.usuarios.logout);
app.get('/api/me', auth.verify, routes.usuarios.me);

app.get('/api/about', routes.empresa.index);

app.get('/api/productos',auth.verify, routes.productos.list);
app.get('/api/productos/buscar', auth.verify, routes.productos.buscar );
// Crear codigo Producto
app.post('/api/productos/crearCodigo', auth.verify, routes.productos.createCodigo )

//app.get('/api/productos', tokenManager.validate, routes.productos.list );

app.get('/api/mis-pedidos', auth.verify, routes.pedidos.misPedidos )
app.get('/api/pedidos/pendientes', auth.verify, routes.pedidos.pendings )
app.delete('/api/pedidos/pendientes', auth.verify, routes.pedidos.deleteAllPending )
app.delete('/api/pedidos/pendientes/:id', auth.verify, routes.pedidos.deletePending )
app.post('/api/pedidos/confirmar', auth.verify, routes.pedidos.confirm )
app.post('/api/pedidos/crear', auth.verify, routes.pedidos.create )


app.get('/api/ping', auth.verify, function(req, res){
    return res.send('pong');
})

// Stock
app.get('/api/stock', auth.verify, routes.stock.find );

app.get('/api/reset', function(req, res){
  res.send('ok');
  process.exit()
})

// Cuenta corriente
app.get('/api/cuenta/find', auth.verify, routes.usuarios.find);
app.post('/api/cuenta/set/:cuenta', auth.verify, routes.usuarios.setCuenta );
app.get('/api/cuenta', auth.verify, routes.cuenta.resumen );

// comprobantes
app.get('/api/comprobante/:tipoId/:numeroId', routes.comprobante.ver );

app.post('/api/upload', upload.single('file'), function (req, res, next) {
  // Obtener los datos de archivos
  var file = req.file;
  var tmp = file.path;
  var excel  = {};
  var result;
  var obj = xlsx.readFile(tmp);
  var name = obj.SheetNames[0];

  var csv = xlsx.utils.sheet_to_csv(obj.Sheets[name]);

  var lines = csv.split("\n"); var result = [];

  var lines = _.filter(lines, function( csvline ) {
      return _.compact(csvline.split(",")).length > 0;
  })

  var worksheet = obj.Sheets[name];
  for (z in worksheet) {
    /* all keys that do not begin with "!" correspond to cell addresses */
    if(z[0] === '!') continue;

    var column = z.replace(/\d+/,'');
    var row    = parseInt( z.replace(/[a-zA-Z]+/,'') ) - 1 ;

    if( typeof result[row] == 'undefined' ) {
      result[row] = {};
    }
    result[row][column] = worksheet[z].v;
  }

  fs.unlink(tmp);
  res.send(result);
});


app.post('/api/upload2', multipartMiddleware, function(req, resp) {
  var file = req.files.file;
  var tmp = file.path;
  var newfile = __dirname + '/files/' + file.name;

  var source = fs.createReadStream(tmp);
  var dest   = fs.createWriteStream(newfile);
  var excel  = {};

  var headers = {
    xls : ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
  };

  source.pipe(dest);

  source.on('end', function(){
    if( _.indexOf(headers.xls, file.headers["content-type"]) > -1 ) {

      var obj = xlsx.readFile(newfile)

      var name = obj.SheetNames[0];

      var csv = xlsx.utils.sheet_to_csv(obj.Sheets[name]);

      var lines = csv.split("\n"); var result = [];

      var lines = _.filter(lines, function( csvline ) {
          return _.compact(csvline.split(",")).length > 0;
      })



      var worksheet = obj.Sheets[name];
      for (z in worksheet) {
        /* all keys that do not begin with "!" correspond to cell addresses */
        if(z[0] === '!') continue;

        var column = z.replace(/\d+/,'');
        var row    = parseInt( z.replace(/[a-zA-Z]+/,'') ) - 1 ;

        if( typeof result[row] == 'undefined' ) {
          result[row] = {};
        }
        result[row][column] = worksheet[z].v;


      }

      console.log(result);
    }
    resp.send(result);
  })

});



// Descargas
app.get('/api/descargas', routes.descargas.index );
app.post('/api/crear-grupo', routes.descargas.addGrupo );


//app.post('/api/descargas/upload', upload.single('file'), routes.descargas.upload )
app.post('/api/descargas/upload', upload.single('file'), function (req, res, next) {
  // Obtener los datos de archivos
  var mongoose = require('mongoose');
  var m        = require('./model/descargas');
  var move     = require('./lib/move');


  function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }

  var file = req.file;
  var tmp = file.path;
  var realFile = guid() + '_' +  file.originalname;
  var newFile  = TabaApp._root + '/../tabaweb/descargas/' + realFile ;

  console.log( file );
  console.log( tmp, newFile );

  move( tmp, newFile, function( err ) {
    if(  err ) {
      console.log( err, tmp, newFile );
      res.send('error');
    } else {
      fs.chmod(newFile, 0777);

      var docFile = new m.File({
        name : file.originalname,
        path : '/descargas/' + realFile,
        size : file.size,
        created : Math.floor(Date.now() / 1000)
      });

      docFile.save(function(e, nFile){

        if( req.body && req.body.group_id ) {
          m.Group.findOne(
            {
              "_id" : new mongoose.Types.ObjectId(req.body.group_id)
            }
          )
          .populate('files')
          .exec(function(e, doc){
            doc.files.push(nFile);
            doc.save();

            return res.send(doc); // Agregar el upload
          });
        } else {
          return res.send(nFile);
        }

      });

    }


  });

});


app.get('/api/descargas/delete/:file_id', routes.descargas.deleteFile )
app.get('/api/download/:file_id', routes.descargas.downloadFile )


// Rutas para el tablero de comando
app.use('/api/tablero', require('./route/tablero'));

/*
app.get('/api/tablero/ventas/dia', function( req, res ){
  var desde = req.query.desde;
  var hasta = req.query.hasta;
  var params = {};
  // Ver si tiene vendedores
  console.log(req.query);
  if( req.query && req.query.vendedores ) {
    params.vendedores = req.query.vendedores
  }

  var model = require('./model/ventas');
  model.dia(desde, hasta, params, function(error, data){

    res.send(data);
  })


})
*/

//Nota de ventas
app.use('/api/nvta', require('./route/notadeventa'));
