// ---------- Variables de control para la aplicacion ---------- //
var cfgApp     = require('./cfg/app');
cfgApp._root   = __dirname;
GLOBAL.TabaApp = cfgApp;

// ---------- Dependencias minimas en el bootstrap ---------- //
var express = require('express'),
        app = express(),
     apiApp = require('./route/api');

// ---------- Definicion de sub app para organizar mejor los datos ---------- //
app.use('/api', apiApp );

// ---------- Inicio de servicio web ---------- //
app.listen(port);
