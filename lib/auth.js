var redisHelper = require('./redisHelper');
var tokenHelper = require('./tokenHelper');
var TIME_TO_LIVE = TabaApp.ttl; //24 hours

//
//  RFC 6750
//  ------------------ Autorizacion basada en tokens ---------------------
//  +--------+                                           +---------------+
//  |        |--(A)------- Authorization Grant --------->|               |
//  |        |                                           |               |
//  |        |<-(B)----------- Access Token -------------|               |
//  |        |               & Refresh Token             |               |
//  |        |                                           |               |
//  |        |                            +----------+   |               |
//  |        |--(C)---- Access Token ---->|          |   |               |
//  |        |                            |          |   |               |
//  |        |<-(D)- Protected Resource --| Resource |   | Authorization |
//  | Client |                            |  Server  |   |     Server    |
//  |        |--(E)---- Access Token ---->|          |   |               |
//  |        |                            |          |   |               |
//  |        |<-(F)- Invalid Token Error -|          |   |               |
//  |        |                            +----------+   |               |
//  |        |                                           |               |
//  |        |--(G)----------- Refresh Token ----------->|               |
//  |        |                                           |               |
//  |        |<-(H)----------- Access Token -------------|               |
//  +--------+           & Optional Refresh Token        +---------------+
//

/*
* Punto de control para verificar el token y obtner la informacion correspondiente,
* ademas este punto de control permite mantener viva la sesion ya que se vuelve a 
* actualizar el tiempo de vida de la variable en redis.
*/
exports.verify = function(req, res, next) {
    var headers = req.headers;
    if (headers == null) return res.send(401);

    // Obtiene el token que se envia en el encabezado
    try {
        var token = tokenHelper.extractTokenFromHeader(headers);
    } catch (err) {
        return res.send(401);
    }

    //Verifica la existencia del token en redis y define el request del middleware
    //             |
    //             v
    // url -> verificacion -> ruteador
    redisHelper.getDataByToken(token, function(err, data) {
        if (err) return res.send(401);

        if( typeof data.Vendedor !== 'undefined' ) {
            data._type = 'vendedor'; 
        } else {
            data._current = {
                Cuenta : data.Cuenta,
                Nombre:  data.Nombre,
                RazonSocial:  data.RazonSocial
            }
            data._type = 'cliente';
        }

        data._token = token;


        req._user = data;
            
        // Refrescar el tiempo de vida del token
        redisHelper.setTokenWithData(token, data, TabaApp.ttl, function(etoken, success){
            next();
        });
        
    });
};

/*
* Crea un nuevo token, y lo guarda en redis con la informacion proporcionada
* con un tiempo de vida en segundos
* 
* @return callback(err, token);
*/
exports.createAndStoreToken = function(data, ttl, callback) {
    data = data || {};
    ttl = ttl || TIME_TO_LIVE;

    if (data != null && typeof data !== 'object') callback(new Error('data is not an Object'));
    if (ttl != null && typeof ttl !== 'number') callback(new Error('ttl is not a valid Number'));

    tokenHelper.createToken(function(err, token) {
        if (err) callback(err);

        redisHelper.setTokenWithData(token, data, ttl, function(err, success) {
            if (err) callback(err);

            if (success) {
                callback(null, token);
            }
            else {
                callback(new Error('Error when saving token'));
            }
        });
    });
};

/*
* Expira el token (lo elimina en redis)
*/
exports.expireToken = function(headers, callback) {
    if (headers == null) callback(new Error('Headers are null'));
    // Get token
    try {
        var token = tokenHelper.extractTokenFromHeader(headers);

        if (token == null) callback(new Error('Token is null'));

        redisHelper.expireToken(token, callback);
    } catch (err) {
        return callback(err);
    }   
}

exports.tokenHelper = tokenHelper;
exports.redisHelper = redisHelper;