module.exports = {
    "_" : require('lodash'),
    "Mustache" : require('mustache'),
    "Conn" : require('./connection'),
    'redis' : require('redis'),
    'multiline' : require('multiline'),
    'events' : require('events'),
    'fs' : require('fs'),
    'crypto' : require('crypto')
}