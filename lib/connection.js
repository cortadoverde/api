// SQL SETTING
var _MSSQL_PARAMS   = require('../cfg/sql');
var sql             = require('mssql');
var events          = require('events');

function Connection( cfg ) {
    this.Driver = sql;
    this.config = cfg;
    this._connected = false;
    this._instance = new sql.Connection(this.config);
}

Connection.prototype = new events.EventEmitter;

Connection.prototype.connect = function( ) {
    var self = this;
    return this._instance.connect()
            .then( function() {
                self._connected = true;
                self.emit('connected');
            })
            .catch( function(e) {
                console.log(e);
                self._connected = false
            })
}

Connection.prototype.Request = function() {
    return new this.Driver.Request( this._instance );
}

var objConn = new Connection(_MSSQL_PARAMS);

exports = module.exports = objConn;
