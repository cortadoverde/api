module.exports = {
    error       : 1,
    advertencia : 2,
    parse       : 4,
    notice      : 8,
    core : {
      error       : 16,
      advertencia : 32,
      parse       : 64,
      notice      : 128,
    },
    user : {
      error : 512
    }

}

// 7
