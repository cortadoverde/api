var redis = require('redis');
var redisClient = redis.createClient(); // por defecto se comunica al puerto 6379
var auth = require('./auth');

// Control interno de conexion al cliente redis
redisClient.on('connect', function () {
    console.log('Redis is ready');
});
 
redisClient.on('error', function(err) {
    throw err;
});

/*
* Se encarga de conectarse al cliente redis para guardar
* los token con la informacion de usuario, lo que permite la portabilidad
* del token sin comprometer informacion.
* el token tiene un tiempo de expiracion que se va renovando a medida que se hacen
* request.
* 
* @param token string
* @param data json
* @param ttl tiempo en segundos
* @param callback function(error, success)
*/
exports.setTokenWithData = function(token, data, ttl, callback) {
    if (token == null) throw new Error('Token is null');
    if (data != null && typeof data !== 'object') throw new Error('data is not an Object');

    var userData = data || {};
    userData._ts = new Date();

    var timeToLive = ttl || auth.TIME_TO_LIVE;
    if (timeToLive != null && typeof timeToLive !== 'number') throw new Error('TimeToLive is not a Number');


    redisClient.setex(token, timeToLive, JSON.stringify(userData), function(err, reply) {
        if (err) callback(err);

        if (reply) {
            callback(null, true);
        } else {
            callback(new Error('Token not set in redis'));
        }
    });
    
};

/*
* Obtiene la informacion asociada a un token
* 
* @param token String
* @param callback function(error, data)
*/
exports.getDataByToken = function(token, callback) {
    if (token == null) callback(new Error('Token is null'));

    redisClient.get(token, function(err, userData) {
        if (err) callback(err);

        if (userData != null) callback(null, JSON.parse(userData));
        else callback(new Error('Token Not Found'));
    });
};

/*
* Elimina el registro del token de redis
* @param token String
* @param callback function(error, success)
*/
exports.expireToken = function(token, callback) {
    if (token == null) callback(new Error('Token is null'));

    redisClient.del(token, function(err, reply) {
        if (err) callback(err);

        if (reply) callback(null, true);
        else callback(new Error('Token not found'));
    });
};

exports.redis = redis;
exports.redisClient = redisClient;