/**
 * Modulo para la creacion de tokens unicos
 */

var crypto = require('crypto');
var TOKEN_LENGTH = 32;
 

/**
 * Crea un nuevo token de 32 caracteres
 * @param  {Function} callback (error, data)
 */
exports.createToken = function(callback) {
    crypto.randomBytes(TOKEN_LENGTH, function(ex, token) {
        if (ex) callback(ex);

        if (token) callback(null, token.toString('hex'));
        else callback(new Error('Problem when generating token'));
    });
};


/**
 * Obtine el token del encabezado, la aplicacion cliente debera mandar
 * en su encabezado el Header "Authorization: Bearer {token]"
 * @param  {Object} header obtenidos con express
 */
exports.extractTokenFromHeader = function(headers) {
    if (headers == null) throw new Error('Header is null');
    if (headers.authorization == null) throw new Error('Authorization header is null');

    var authorization = headers.authorization;
    var authArr = authorization.split(' ');
    if (authArr.length != 2) throw new Error('Authorization header value is not of length 2');

    // retrieve token
    var token = authArr[1]; 
    if (token.length != TOKEN_LENGTH * 2) throw new Error('Token length is not the expected one');

    return token;
};
