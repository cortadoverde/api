var lib = require('../lib/common');

lib.Conn.connect();

var modelo = {

    getComprobante: function ( tipoId, numeroId, callback ) {
        if( lib.Conn._connected ) {

            var request = lib.Conn.Request();

            request.input('tipoID', lib.Conn.Driver.Int, tipoId );
            request.input('numeroId', lib.Conn.Driver.Int, numeroId );

            var q = lib.Mustache.render(
                        lib.multiline(function(){/*
                            SELECT
                                TablaDeComprobantes.NombreImprimir,
                                Facturas.Cuenta,
                                Facturas.Comprobante,
                                Facturas.FechaSql,
                                Facturas.RazonSocial,
                                Facturas.Domicilio,
                                Facturas.CodigoPostal,
                                Facturas.ImporteSubtotalPanta,
                                Facturas.ImporteDescuento,
                                Facturas.ImporteIVA,
                                Facturas.ImporteCbte,
                                Facturas.BonificacionAcordada,
                                Facturas.BonificacionProntoPago
                            FROM
                                TablaDeComprobantes
                            INNER JOIN
                                Facturas
                                    ON
                                        TablaDeComprobantes.TIPO = Facturas.TIPOID
                            WHERE
                                Facturas.TIPOID = @tipoId AND Facturas.NUMEROID = @numeroId
                        */}),
                        TabaApp.empresa
                    );
            request
                .query(q)
                .then( function (recordset) {
                    modelo.getComprobanteMov( tipoId, numeroId, function( err, movimientos ) {
                        recordset[0].movimientos = movimientos;
                        return callback( null, recordset[0]);
                    })
                })
                .catch( function (err) {
                    return callback( err, null )
                })
        } else {
            return callback({ error : ' no se puede conectar '}, null);
        }
    },

    getComprobanteMov: function ( tipoId, numeroId, callback ) {
        if( lib.Conn._connected ) {

            var request = lib.Conn.Request();

            request.input('tipoID', lib.Conn.Driver.Int, tipoId );
            request.input('numeroId', lib.Conn.Driver.Int, numeroId );

            var q = lib.Mustache.render(
                        lib.multiline(function(){/*
                            SELECT
                                FacturasMov.CodProducto,
                                FacturasMov.Cantidad,
                                Productos.Nombre,
                                FacturasMov.ImporteFac
                            FROM
                                FacturasMov
                            INNER JOIN
                                Productos
                                    ON
                                        FacturasMov.CODPRODUCTO = Productos.CODPRODUCTO
                            WHERE
                                FacturasMov.TIPOID = @tipoId AND FacturasMov.NUMEROID = @numeroId
                        */}),
                        TabaApp.empresa
                    );

            request
                .query(q)
                .then( function (recordset) {
                    return callback( null, recordset );
                })
                .catch( function (err) {
                    return callback( err, null )
                })

        } else {
            return callback({ error : ' no se puede conectar '}, null);
        }
    }
}


module.exports = modelo;
