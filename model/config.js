var   redisHelper = require('../lib/redisHelper');
var             _ = require('lodash');
var      Mustache = require('mustache');
var            fs = require('fs');
var        events = require('events');
var        crypto = require('crypto');
var         redis = require('redis');
var   redisClient = redis.createClient();

var Connfiguration = function( namespace ) {
    this.ns     = namespace;
    this._store = false;
    this.data   = {};
    this.paths  = {
        cache : __dirname + '/../cfg/_cache'
    }
    this._file  = this.paths.cache + '/' + this.ns + '.json';
    this.hash   = this._currentHash();
    this.run();
} 

Connfiguration.prototype = new events.EventEmitter;

Connfiguration.prototype._currentHash = function() {
    return crypto.createHash('md5').update(this._file).digest('hex');
}

Connfiguration.prototype.run = function( ) {
    // si no existe el archivo crea uno vacio
    if ( !fs.existsSync ( this._file ) ) {
        redisClient.get(this.hash, function(error, data){
            this.create(JSON.parse(data), function(){});
        });        
    } else {
         var file = JSON.parse( fs.readFileSync( this._file ) );
         redisClient.set( this.hash, JSON.stringify(file) );
         this.data = file; 
    }
}

Connfiguration.prototype.load = function( callback ) {
    var self = this;

    redisClient.get(this.hash, function(error, data){
        if( error ) {
            if ( fs.existsSync ( self._file ) ) {
                var file = JSON.parse( fs.readFileSync( self._file ) );
                redisClient.set( self.hash, JSON.stringify(file) )
                self.data = file;
                self.emit('data', file);
                return callback( null, file );
            } else {
                self.emit('error', error );
                return callback( error, null );
            }
        }

        if ( ! data ) {
            if ( fs.existsSync ( self._file ) ) {
                var file = JSON.parse( fs.readFileSync( self._file ) );
                redisClient.set( self.hash, JSON.stringify(file) )
                self.data = file;
                self.emit('data', file);
                return callback( null, file );
            }
        }
        self.data = JSON.parse(data);
        self.emit('data', JSON.parse(data));
        return callback(null, JSON.parse(data) );
        
    });    
    
}

Connfiguration.prototype.delete = function ( callback ) {
    var self = this;
    redisClient.del( this.hash, function ( error, reply ) {
        if( ! error ) {
            fs.unlinkSync( self._file );
            self.emit('delete');
            self.data = null;
            if( reply ) { 
                return callback(error, reply)
            } else {
                return callback(new Error('No se encuentra el hash'), null);
            }
        }
        self.emit('error', error);
        return callback(error, reply)
    })
} 

Connfiguration.prototype.merge = function( newData, callback ) {
    var self = this;
    this.data = _.merge(this.data, newData);
    this.create(this.data, function(err, resp){
        if( err ) {
            self.emit('error', err );
            return callback(err, null);
        }

        redisClient.set(self.hash, JSON.stringify(self.data));
        return callback(null, true);
    })
}

Connfiguration.prototype.update = function( callback ) {
    redisClient.set(this.hash, JSON.stringify(this.data));
    return callback(null, true);
}

Connfiguration.prototype.create = function( data, callback ) {
    var txt = JSON.stringify(data, null, 4);
    var self = this;
    fs.writeFile(this._file, txt, function( err ){
        if( err ) {
            self.emit('error', err );
            return callback(err, null);
        }
        return callback(null, true);
    })

}

module.exports = Connfiguration;