var lib = require('../lib/common');

lib.Conn.connect();

var modelo = {

    getPending: function ( cuenta, callback ) {
        if( lib.Conn._connected ) {

            var request = lib.Conn.Request();

            request.input('cuenta', lib.Conn.Driver.VarChar, cuenta );

            var q = lib.Mustache.render(
                        lib.multiline(function(){/*
                            SELECT
                                MovCtasCtes.TipoId,
                                MovCtasCtes.NumeroId,
                                TablaDeComprobantes.clasecbte,
                                MovCtasCtes.VENCESQL VenceSql,
                                TablaDeComprobantes.ABREVIATURA Abreviatura,
                                Facturas.COMPROBANTE Comprobante,
                                Facturas.FECHASQL FechaSql,
                                EST =
                                        CASE MovCtasCtes.ESTADO
                                            WHEN 1 THEN 'PEN'
                                            WHEN 3 THEN 'CTA'
                                            ELSE ''
                                        END,
                                MovCtasCtes.TOTAL Total,
                                MovCtasCtes.SALDO Saldo
                            FROM
                                MovCtasCtes
                            INNER JOIN
                                Facturas
                                    ON
                                        MovCtasCtes.TIPOID = Facturas.TIPOID
                                            AND
                                        MovCtasCtes.NUMEROID = Facturas.NUMEROID
                            INNER JOIN
                                TablaDeComprobantes
                                    ON
                                        Facturas.TIPOID = TablaDeComprobantes.TIPO
                            WHERE
                                MovCtasCtes.CUENTA = @cuenta
                                    AND
                                MovCtasCtes.ESTADO >= 1
                                    AND
                                MovCtasCtes.ESTADO <= 3
                                    AND
                                (
                                    MovCtasCtes.SubCtaCte = {{ subcta1 }}
                                        OR
                                    MovCtasCtes.SubCtaCte = {{ subcta2 }}
                                        OR
                                    MovCtasCtes.SubCtaCte = {{ subcta3 }}
                                        OR
                                    MovCtasCtes.SubCtaCte = {{ subcta4 }}
                                        OR
                                    MovCtasCtes.SubCtaCte = {{ subcta5 }}
                                        OR
                                    MovCtasCtes.SubCtaCte = {{ subcta6 }}
                                )
                            ORDER BY
                                MovCtasCtes.CUENTA,
                                MovCtasCtes.ESTADO,
                                MovCtasCtes.VENCESQL,
                                MovCtasCtes.TIPOID,
                                MovCtasCtes.NUMEROID,
                                MovCtasCtes.CUOTA
                        */}),
                        TabaApp.empresa
                    );
            request
                .query(q)
                .then( function (recordset) {
                    return callback( null, recordset );
                })
                .catch( function (err) {
                    return callback( err, null )
                })
        } else {
            return callback({ error : ' no se puede conectar '}, null);
        }
    },

    getResumen: function ( cuenta, callback ) {
        if( lib.Conn._connected ) {

            var request = lib.Conn.Request();

            request.input('cuenta', lib.Conn.Driver.VarChar, cuenta );

            var q = lib.Mustache.render(
                        lib.multiline(function(){/*
                            SELECT
                                Facturas.CUENTA Cuenta,
                                Facturas.FECHASQL FechaSql,
                                TablaDeComprobantes.ABREVIATURA Abreviatura,
                                Facturas.COMPROBANTE Comprobante,
                                SubCtaCte.NOMBRE Nombre,
                                Facturas.IMPORTECBTE ImporteCbte,
                                Facturas.ESCONTADO EsContado,
                                TablaDeComprobantes.clasecbte ClaseCbte,
                                Facturas.tipoid TipoId,
                                Facturas.numeroid NumeroId
                            FROM
                                Facturas
                            INNER JOIN
                                TablaDeComprobantes
                                    ON
                                        Facturas.TIPOID = TablaDeComprobantes.TIPO
                            INNER JOIN
                                SubCtaCte
                                    ON
                                        Facturas.SUBCTACTE = SubCtaCte.SUBCTACTE
                            WHERE
                                fechasql > ( getdate()-{{ diasctacte }} )
                                    AND
                                Facturas.CUENTA = @cuenta
                                    AND
                                Facturas.FECHAANULACIONSQL IS NULL
                                    AND
                                (
                                    Facturas.SubCtaCte = {{ subcta1 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta2 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta3 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta4 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta5 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta6 }}
                                )
                            ORDER BY
                                Facturas.CUENTA,
                                Facturas.FECHASQL,
                                Facturas.TIPOID
                        */}),
                        TabaApp.empresa
                    );
            request
                .query(q)
                .then( function (recordset) {
                    modelo.getSaldo(cuenta, function(err, saldo ){
                        var data = {
                            SaldoInicial : saldo[0].SaldoInicial,
                            Items : recordset
                        }
                        return callback( null, data );
                    })
                })
                .catch( function (err) {
                    return callback( err, null )
                })
        } else {
            return callback({ error : ' no se puede conectar '}, null);
        }
    },

    getSaldo: function( cuenta, callback) {
        if( lib.Conn._connected ) {
            var request = lib.Conn.Request();

            request.input('cuenta', lib.Conn.Driver.VarChar, cuenta );

            var q = lib.Mustache.render(
                        lib.multiline(function(){/*
                            SELECT
                                sum(Facturas.IMPORTECBTE) as SaldoInicial
                            FROM
                                Facturas
                            INNER JOIN
                                TablaDeComprobantes
                                    ON
                                        Facturas.TIPOID = TablaDeComprobantes.TIPO
                                            AND
                                        Facturas.ESCONTADO=0
                            INNER JOIN
                                SubCtaCte
                                    ON
                                        Facturas.SUBCTACTE = SubCtaCte.SUBCTACTE
                            WHERE
                                fechasql <= (getdate()-{{diasctacte}})
                                    AND
                                Facturas.CUENTA = @cuenta
                                    AND
                                Facturas.FECHAANULACIONSQL IS NULL
                                    AND
                                (
                                    Facturas.SubCtaCte = {{ subcta1 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta2 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta3 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta4 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta5 }}
                                        OR
                                    Facturas.SubCtaCte = {{ subcta6 }}
                                )
                        */}),
                        TabaApp.empresa
                    );
            request
                .query(q)
                .then( function (recordset) {
                    return callback( null, recordset );
                })
                .catch( function (err) {
                    return callback( err, null )
                })
        } else {
            return callback({ error : ' no se puede conectar '}, null);
        }
    }
}


module.exports = modelo;
