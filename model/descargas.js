var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var groupSchema = new Schema ({
  title : String,
  order : Number,
  icon  : String,
  files : [{ type: Schema.ObjectId, ref : 'file' }]
})

var fileSchema = new Schema ({
  name : String,
  mime : String,
  path : String,
  filetype: String,
  created: Date,
  size: Number
});

module.exports = {
  'Group' : mongoose.model('group', groupSchema),
  'File'  : mongoose.model('file', fileSchema)
}
