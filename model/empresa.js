var conn    = require('../lib/connection');
var    _    = require('lodash');
var configModel  = require('./config');

conn.connect();

var config = new configModel('empresa');

var modelo = {

    get : function( callback ) {



        if( conn._connected ) {
          var request = new conn.Driver.Request(conn._instance);
          request
            .query('SELECT TOP 1 * From empresa', function ( err, recordset ){
              var data = require('../cfg/app');
              var row  = recordset[0];


              var result = _.merge(
                              {
                                EMPRESA : row.EMPRESA,
                                Domicilio : row.DOMICILIO,
                                CUIT: row.CUIT,
                                IngBrutos : row.INGBRUTOS,
                                Stock : row.STOCK
                              },
                              data.empresa
                            );

               return callback(err, result);

            })
        }
    }
}


module.exports = modelo;
