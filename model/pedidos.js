var conn = require('../lib/connection');
var _ = require('lodash');
var multiline = require('multiline');
var Mustache = require('mustache');

conn.connect();

var modelo = {

    getLastOrderByCuenta : function( cuenta, callback ) {
            if( conn._connected ) {
                var request = new conn.Driver.Request(conn._instance);

                request.input('CUENTA', conn.Driver.VarChar, cuenta);

                var q = [
                    'SELECT ',
                        ' npMov.FECHASQL Fecha, ',
                        ' npMov.CODPRODUCTO CodProducto, ',
                        ' Productos.Nombre, ',
                        ' npMov.CantidadPedida Cantidad, ',
                        ' npMov.Detalle, ',
                        ' npMov.Estado ',
                    ' FROM ',
                        'NotasDePedidosMov npMov',
                    ' INNER JOIN Productos ',
                        ' ON Productos.CodProducto = npMov.CodProducto ',
                    ' WHERE ',
                        ' ESTADO > 1 and CUENTA = @CUENTA ',
                        '   AND  ',
                        ' ( Tipoid=0 OR ( npMov.fechasql > GETDATE() - 30 ) ) ',
                    ' ORDER BY ',
                        ' Estado ASC, fechasql DESC, NotasDePedidosMov.CODPRODUCTO ASC'
                ].join('');


                request
                    .query(q)
                    .then( function (recordset) {
                        callback( null, recordset );
                    })
                    .catch( function (error) {
                        callback( error, null );
                    })

            } else {
                callback({ error : ' no se puede conectar '}, null);
            }
    },

    getPendingByCuenta : function( cuenta, callback ) {
            if( conn._connected ) {
                var request = new conn.Driver.Request(conn._instance);

                request.input('CUENTA', conn.Driver.VarChar, cuenta);
                    // SELECT FECHASQL, NotasDePedidosMov.CODPRODUCTO,Nombre ,CANTIDADPEDIDA, DETALLE,TipoID,Registro FROM NotasDePedidosMov inner join productos on NotasDePedidosMov.codproducto=productos.codproducto WHERE CUENTA = ? AND ESTADO=1 AND (Tipoid=0 or (notasdepedidosmov.fechasql>GETDATE()-30)) order by fechasql,NotasDePedidosMov.CODPRODUCTO
                var q = [
                    'SELECT ',
                        ' npMov.FECHASQL Fecha, ',
                        ' npMov.CODPRODUCTO CodProducto, ',
                        ' Productos.Nombre, ',
                        ' npMov.CantidadPedida Cantidad, ',
                        ' npMov.Detalle, ',
                        ' npMov.Registro, ',
                        ' npMov.Estado, ',
                        ' npMov.BONIFICACIONSUGERIDA Descuento ',
                    ' FROM ',
                        'NotasDePedidosMov npMov',
                    ' INNER JOIN Productos ',
                        ' ON Productos.CodProducto = npMov.CodProducto ',
                    ' WHERE ',
                        ' ESTADO =1  and CUENTA = @CUENTA ',
                        '   AND  ',
                        ' ( Tipoid=0 OR ( npMov.fechasql > GETDATE() - 30 ) ) ',
                    ' ORDER BY ',
                        'REGISTRO'
                ].join('');


                request
                    .query(q)
                    .then( function (recordset) {
                        callback( null, recordset );
                    })
                    .catch( function (error) {
                        callback( error, null );
                    })

            } else {
                callback({ error : ' no se puede conectar '}, null);
            }
    },

    deleteAllPendingsByCuenta : function( cuenta, callback ) {
        if( conn._connected ) {
            var request = new conn.Driver.Request(conn._instance);
            request.input('CUENTA', conn.Driver.VarChar, cuenta);

            var q = [
                    'DELETE ',
                    ' FROM ',
                        'NotasDePedidosMov',
                    ' WHERE ',
                        ' ESTADO = 1  and CUENTA = @CUENTA '
                ].join('');

                request
                    .query(q)
                    .then( function (recordset) {
                        callback( null, recordset );
                    })
                    .catch( function (error) {
                        callback( error, null );
                    })

        } else {
            callback({ error : ' no se puede conectar '}, null);
        }
    },

    deletePending: function( registro, cuenta, callback ) {
        if( conn._connected ) {
            var request = new conn.Driver.Request(conn._instance);
            request.input('CUENTA', conn.Driver.VarChar, cuenta);
            request.input('REGISTRO', conn.Driver.VarChar, registro);

            var q = [
                    'DELETE ',
                    ' FROM ',
                        'NotasDePedidosMov ',
                    ' WHERE ',
                        ' REGISTRO = @REGISTRO  and CUENTA = @CUENTA '
                ].join('');

            request
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function (error) {
                    callback( error, null );
                })

        } else {
            callback({ error : ' no se puede conectar '}, null);
        }
    },

    confirm : function( cuenta, callback ) {
        // UPDATE dbo.NotasDePedidosMov set ESTADO=2 WHERE CUENTA='" & maxi1__parametro1 & "' AND ESTADO=1
        if( conn._connected ) {
            var request = new conn.Driver.Request(conn._instance);
            request.input('CUENTA', conn.Driver.VarChar, cuenta);

            var q = [
                    'UPDATE ',
                        'NotasDePedidosMov',
                    ' SET ',
                        ' ESTADO = 2 ',
                    ' WHERE ',
                        ' CUENTA = @CUENTA AND ESTADO = 1'
                ].join('');

            request
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function (error) {
                    callback( error, null );
                })

        } else {
            callback({ error : ' no se puede conectar '}, null);
        }

    },

    /**
     * Crea el pedido a partir del excel,
     */
    create : function( cuenta, data, callback ) {
        var current = [];
        var errors  = [];
        var i = -1;
        var n = 0;
        modelo.getNextId(cuenta, function(error, result) {
            if( i == -1 ) {
                i = result[0].maximo;
            }

            _.forEach(data, function(obj, key, arr){
                    obj.CodProducto = key.trim();

                    obj.renglon = i;
                    i++;

                    modelo.existeProducto(cuenta, key, obj, function(error, reg) {
                        if( ! error ){

                          console.log('Reg: ', reg);

                          if( reg._valid )
                            modelo.insertNotaVenta(cuenta, reg, function( errror, recordset ){
                              if( error )
                                errors.push({
                                  header: "Error al crear el codigo",
                                  err: error,
                                  message: error
                                });
                                console.log(error);
                            });
                          else
                            errors.push({
                                reg : n,
                                original: reg,
                                codProducto : key,
                                header: "Codigo no detectado: " + key,
                                message: 'No se ha encontrado el código de producto o sinonimo'
                            });
                        } else {
                          errors.push({
                            reg: n,
                            original:obj,
                            header: "Error interno",
                            message: error
                          });
                        }

                        if( n === _.keys(data).length - 1 ) {
                          setTimeout(function(){
                            callback(null, errors);
                          } , 150 );
                        }
                        n++;
                    });


            });

        })


    },

    existeProducto : function(cuenta, codigo, obj,  callback ) {
        if( conn._connected ) {
            // Crea un request para hacer la primer consulta
            var request = new conn.Driver.Request(conn._instance);

            // Filtro para que solo se ingresen productos que estan autorizados a mostrarse en la web
            var filtroProdutos = ' AND '
                               + ' Productos.TabaOnLine = 1 AND Productos.TABAONLINEFECHAPUBSQL <= GETDATE() AND Productos.Perfil <= 2 '
            ;

            // Consulta por codigo de producto
            var q   = ' SELECT COUNT(*) n FROM Productos '
                    + ' WHERE '
                    + ' CODPRODUCTO = @codigo '
                    + filtroProdutos
            ;

            // Consulta por codigo de cliente
            var qcc = ' SELECT Productos.CODPRODUCTO FROM ProductosClientes '
                    + ' INNER JOIN Productos ON Productos.CODPRODUCTO = ProductosClientes.CODPRODUCTO '
                    + ' WHERE '
                    + ' ProductosClientes.CUENTA = @cuenta AND ProductosClientes.CODPRODUCTOCLIENTE = @codigo '
                    + filtroProdutos
            ;

            // Consulta por sinonimo de empresa
            var qs  = ' SELECT CODPRODUCTO FROM Productos '
                    + ' WHERE '
                    + ' SINONIMO = @codigo '
                    + filtroProdutos
            ;

            // Escapar valores sensibles
            request.input( 'codigo', conn.Driver.VarChar, codigo );
            request.input( 'cuenta', conn.Driver.VarChar, cuenta );

            // Validación
            obj._valid = false;

            request
                .query(q)
                .then( function (recordset) {

                    // Existe el codigo de producto, tal cual esta, asi que se devuelve el objeto
                    // para procesarlo
                    if( recordset[0].n == 1 ) {
                        obj._valid = true;
                        return callback( null, obj );
                    }

                    // Busqueda por codigo de cliente
                    var ccrequest   = new conn.Driver.Request(conn._instance);
                        ccrequest.input( 'codigo', conn.Driver.VarChar, codigo );
                        ccrequest.input( 'cuenta', conn.Driver.VarChar, cuenta );

                    ccrequest
                      .query(qcc, function( e, ccrecordset ) {
                            if( e ) return callback( true, null );

                            if( ccrecordset.length > 0 ) {
                                var item = ccrecordset[0];
                                obj.CodProducto = item.CODPRODUCTO;
                                obj._valid = true;
                                return callback( null, obj );
                            } else {
                                var srequest = new conn.Driver.Request(conn._instance);
                                  srequest.input( 'codigo', conn.Driver.VarChar, codigo );
                                  srequest.input( 'cuenta', conn.Driver.VarChar, cuenta );

                                  srequest
                                    .query(qs, function( e, srecordset ) {
                                          if( e ) return callback( true, null );

                                          if( srecordset.length > 0 ) {
                                            var item = srecordset[0];
                                            obj.CodProducto = item.CODPRODUCTO;
                                            obj._valid = true;
                                            return callback( null, obj );
                                          } else {
                                            // No existe de ningun modo
                                            return callback( null, obj );
                                          }

                                    });
                            }

                      });
                })
                .catch( function (error) {
                    return callback( true, null );
                });

        } else {
            return callback( true, null );
        }
    },

    insertNotaVenta : function( cuenta, data, callback ) {

               if( conn._connected ) {
                    var request = new conn.Driver.Request(conn._instance);

                    descuento = ( typeof data.descuento != 'undefined' ) ? data.descuento : 0;
                    descuento2 = ( typeof data.descuento2 != 'undefined' ) ? data.descuento2 : 0;

                    request.input('CUENTA', conn.Driver.VarChar, cuenta);
                    request.input('CodProducto', conn.Driver.VarChar, data.CodProducto);
                    request.input('Cantidad', conn.Driver.VarChar, data.cantidad);
                    request.input('Detalle', conn.Driver.VarChar, data.detalle);
                    request.input('NextID', conn.Driver.VarChar, data.renglon);
                    request.input('Descuento', conn.Driver.VarChar, descuento );
                    request.input('Descuento2', conn.Driver.VarChar, descuento2 );


                    var q = [
                            'INSERT INTO ',
                                'NotasDePedidosMov',
                            ' (CODPRODUCTO, CUENTA, RENGLON, TIPOID, NUMEROID, CANTIDADPEDIDA, FECHASQL, FECHAENTREGASQL, DETALLE, BONIFICACION, BONIFICACION1, BONIFICACIONSUGERIDA, REGISTRO,ESTADO) ',
                                ' VALUES (',
                            ' @CodProducto, ',
                            ' @Cuenta, ',
                            ' 0, ',
                            ' 0, ',
                            ' 0, ',
                            ' @Cantidad, ',
                            ' GetDate() , ',
                            ' GetDate() , ',
                            ' @Detalle, ',
                            ' @Descuento, ',
                            ' @Descuento2, ',
                            ' @Descuento, ',
                            ' @NextID , ',
                            ' 1 ) '

                        ].join('');

                    console.log(q);

                    request
                        .query(q)
                        .then( function (recordset) {
                            callback( null, recordset );
                        })
                        .catch( function (error) {
                            console.log(error);
                            callback( error, null );
                        })

                } else {
                    callback({ error : ' no se puede conectar '}, null);
                }



    },

    getNextId : function( cuenta, callback ) {
        //SELECT case when max(registro) is null then 1 else max(registro)+1 end  as maximo  FROM NotasDePedidosMov  WHERE cuenta=
        if( conn._connected ) {
            var request = new conn.Driver.Request(conn._instance);
            request.input('CUENTA', conn.Driver.VarChar, cuenta);

            var q = [
                    ' (SELECT case when max(registro) is null then 1 else max(registro)+1 end  as maximo  FROM NotasDePedidosMov  WHERE CUENTA = @CUENTA )'
                ].join('');

            request
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function (error) {
                    callback( error, null );
                })

        } else {
            callback({ error : ' no se puede conectar '}, null);
        }
    }

    // SELECT FECHASQL, NotasDePedidosMov.CODPRODUCTO,Nombre ,CANTIDADPEDIDA, DETALLE,TipoID,Registro,estado FROM NotasDePedidosMov inner join productos on NotasDePedidosMov.codproducto=productos.codproducto WHERE ESTADO>1 and CUENTA = ? AND (Tipoid=0 or (notasdepedidosmov.fechasql>GETDATE()-30)) order by estado asc,fechasql desc,NotasDePedidosMov.CODPRODUCTO asc
}

module.exports = modelo;
