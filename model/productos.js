var
       cfg  = require('../cfg/sql'),
       sql  = require('mssql');

var _ = require('lodash');
var multiline = require('multiline');
var Mustache = require('mustache')
var conn = require('../lib/connection');

conn.connect();


var modelo = {

    listAll : function( callback ) {
        sql.connect(cfg, function(err){
            var request = new sql.Request();

            // Obtener produtos

            var q   = ' SELECT Productos.CodProducto, Productos.Nombre, NombreAdicional'
                    + ' FROM Productos '
                    + ' WHERE '
                    + ' TabaOnLine = 1 AND TABAONLINEFECHAPUBSQL <= GETDATE() AND Perfil <= 2'
                    + '( productos.perfil = 1 OR Productos.perfil = 2 ) '
                    + ' ORDER BY Productos.Nombre, Productos.CodProducto '
                    ;
            request.query(q, function( err, recordset ){
                return callback(err, recordset);
            })


        })
    },

    find: function( type, str, cuenta, callback ) {
        if( type == 'codProducto')
            return modelo.findByCodProducto(cuenta, str, callback);
        return modelo.findByCodProductoCliente(cuenta, str, callback);
    },

    findByCodProducto : function( cuenta, producto, callback ) {
        if( conn._connected ) {
                var request = new conn.Driver.Request(conn._instance);
                var productVars = {
                    conds : []
                };

                request.input('productol', conn.Driver.VarChar, '%' + producto + '%');
                request.input('producto', conn.Driver.VarChar, producto );
                request.input('cuenta', conn.Driver.VarChar, cuenta );

                producto.split(' ').forEach(function(str, idx){
                    productVars.conds.push({
                        str : 'subproducto_' + idx,
                        idx : idx,
                        val : str
                    });

                    request.input('subproducto_' + idx, conn.Driver.VarChar, '%' + str + '%');
                })

                    // SELECT FECHASQL, NotasDePedidosMov.CODPRODUCTO,Nombre ,CANTIDADPEDIDA, DETALLE,TipoID,Registro FROM NotasDePedidosMov inner join productos on NotasDePedidosMov.codproducto=productos.codproducto WHERE CUENTA = ? AND ESTADO=1 AND (Tipoid=0 or (notasdepedidosmov.fechasql>GETDATE()-30)) order by fechasql,NotasDePedidosMov.CODPRODUCTO
                var q = multiline(function(){/*
                        SELECT
                            RTRIM(Productos.CodProducto) CodProducto,
                            Productos.Nombre ,
                            NombreAdicional,
                            Lista.Precio ,
                            stockactual1 =
                                CASE WHEN Stock.STOCKACTUAL IS NULL
                                    THEN 0
                                    ELSE stock.stockactual
                                END,
                            stockminimo =
                                CASE WHEN Stock.STOCKMINIMO IS NULL
                                    THEN 0 ELSE stock.stockminimo
                                END,
                            Pc.CodProductoCliente,
                            Pc.Nombre CodClienteNombre
                        FROM
                        Productos
                            LEFT JOIN
                                Stock ON {{ deposito }} = Stock.DEPOSITO  AND Stock.CODPRODUCTO = Productos.CODPRODUCTO

                            LEFT JOIN
                                ListasPreciosPrecios AS Lista ON Lista.LISTA = 1 AND Lista.CodProducto = Productos.CodProducto
                            LEFT JOIN
                                ProductosClientes Pc
                                    ON
                                        Pc.CodProducto = Productos.CodProducto
                                            AND
                                        Pc.Cuenta = @cuenta
                            WHERE

                                Productos.TabaOnLine = 1 AND Productos.TABAONLINEFECHAPUBSQL <= GETDATE() AND Productos.Perfil <= 2

                                {{# conds }}

                                    and
                                (
                                    Productos.codproducto like  @{{ str }}
                                        OR
                                    Productos.nombre like @{{ str }}
                                        OR
                                    Productos.sinonimo like @{{ str }}
                                )

                                {{/ conds }}

                          ORDER BY Productos.Nombre, Productos.CodProducto
                */});

                var renderVars = _.extend(TabaApp.empresa, productVars);

                q = Mustache.render(q, renderVars);

                request
                    .query(q)
                    .then( function (recordset) {
                        callback( null, recordset );
                    })
                    .catch( function (error) {
                        callback( error, null );
                    })

            } else {
                callback({ error : ' no se puede conectar '}, null);
            }
    },

    findByCodProductoCliente : function( cuenta, producto, callback ) {
        if( conn._connected ) {
                var request = new conn.Driver.Request(conn._instance);
                var productVars = {
                    conds : []
                };
                request.input('productol', conn.Driver.VarChar, '%' + producto + '%');
                request.input('cuenta', conn.Driver.VarChar, cuenta);
                request.input('producto', conn.Driver.VarChar, producto );

                producto.split(' ').forEach(function(str, idx){
                    productVars.conds.push({
                        str : 'subproducto_' + idx,
                        idx : idx,
                        val : str
                    });

                    request.input('subproducto_' + idx, conn.Driver.VarChar, '%' + str + '%');
                })

                    // SELECT FECHASQL, NotasDePedidosMov.CODPRODUCTO,Nombre ,CANTIDADPEDIDA, DETALLE,TipoID,Registro FROM NotasDePedidosMov inner join productos on NotasDePedidosMov.codproducto=productos.codproducto WHERE CUENTA = ? AND ESTADO=1 AND (Tipoid=0 or (notasdepedidosmov.fechasql>GETDATE()-30)) order by fechasql,NotasDePedidosMov.CODPRODUCTO
                var q = multiline(function(){/*
                    SELECT
                            RTRIM(Productos.CodProducto) CodProducto,
                            Productos.Nombre ,
                            NombreAdicional,
                            Lista.Precio ,
                            stockactual1 =
                                CASE WHEN Stock.STOCKACTUAL IS NULL
                                    THEN 0
                                    ELSE stock.stockactual
                                END,
                            stockminimo =
                                CASE WHEN Stock.STOCKMINIMO IS NULL
                                    THEN 0 ELSE stock.stockminimo
                                END
                        FROM
                            Productos
                        INNER JOIN
                            ProductosClientes
                                ON
                                    ProductosClientes.codproducto = Productos.codproducto
                                        AND
                                    ProductosClientes.Cuenta = @cuenta
                        LEFT JOIN
                            Stock ON {{ deposito }} = Stock.DEPOSITO  AND Stock.CODPRODUCTO = Productos.CODPRODUCTO

                        LEFT JOIN
                            ListasPreciosPrecios AS Lista ON Lista.LISTA = 1 AND Lista.CodProducto = Productos.CodProducto

                        WHERE

                            Productos.TabaOnLine = 1 AND Productos.TABAONLINEFECHAPUBSQL <= GETDATE() AND Productos.Perfil <= 2

                            {{# conds }}

                                and
                            (
                                ProductosClientes.CODPRODUCTOCLIENTE like  @{{ str }}
                                    OR
                                ProductosClientes.Nombre like @{{ str }}
                            )

                            {{/ conds }}

                        ORDER BY Productos.Nombre, Productos.CodProducto
                */});


                var renderVars = _.extend(TabaApp.empresa, productVars);
                q = Mustache.render(q, renderVars);


                request
                    .query(q)
                    .then( function (recordset) {
                        callback( null, recordset );
                    })
                    .catch( function (error) {
                        callback( error, null );
                    })

            } else {
                callback({ error : ' no se puede conectar '}, null);
            }
    },

    createCodigoCliente : function( cuenta, data, callback ) {
        var error = null;

        _.forEach(data, function(obj, key){
            var insert = {
                cuenta : cuenta,
                codProducto : key,
                CodProductoCliente: obj.codigo,
                nombre : obj.nombre
            }

            modelo.insertItemCodPersonalizado( insert, function( e, data ) {
                if( e ) {
                    error = e;
                }
            } );

        });

        callback(error, null);
    },

    insertItemCodPersonalizado : function( data , callback ) {
        if( conn._connected ) {
            var request = new conn.Driver.Request(conn._instance);

            request.input('cuenta', conn.Driver.VarChar, data.cuenta);
            request.input('codProducto', conn.Driver.VarChar, data.codProducto);
            request.input('CodProductoCliente', conn.Driver.VarChar, data.CodProductoCliente);
            request.input('nombre', conn.Driver.VarChar, data.nombre);

            if( data.CodProductoCliente == '' ) {
                return modelo.deleteCodPersonalizado( data.codProducto, data.cuenta, callback );
            }

            var q = multiline(function(){/*
                INSERT INTO
                    ProductosClientes
                ( CODPRODUCTO, CUENTA, CODPRODUCTOCLIENTE, NOMBRE )
                    VALUES
                ( @codProducto, @cuenta, @CodProductoCliente, @nombre )

            */});

            // Evitar que se dupliquen los codigos personalizados
            modelo.deleteCodPersonalizado(data.codProducto, data.cuenta, function(e, s){
                request
                    .query(q)
                    .then( function (recordset) {
                        callback( null, recordset );
                    })
                    .catch( function (error) {
                        callback( error, null );
                    })
            })


        } else {
            callback({ error : ' no se puede conectar '}, null);
        }
    },

    deleteCodPersonalizado : function( codProducto, cuenta, callback ) {
        if( conn._connected ) {

            var request = new conn.Driver.Request(conn._instance);

            request.input('cuenta', conn.Driver.VarChar, cuenta);
            request.input('codProducto', conn.Driver.VarChar, codProducto);

            var q = 'DELETE FROM ProductosClientes WHERE cuenta = @cuenta AND codProducto = @codProducto ';

            request
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function (error) {
                    callback( error, null );
                })

        } else {
            callback({ error : ' no se puede conectar '}, null);
        }
    }
}


module.exports = modelo;
