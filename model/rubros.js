var
       cfg  = require('../cfg/sql'),
       sql  = require('mssql'),
    extend  = require('extend');

var default_params = {};

function Model( params ) {
  this.params = extend(default_params, params || {} );
}

Model.prototype = {
  __isConnected : false,
  // Wrap para evitar redundancia en la ejecucion del sql
  __runSql : function( fn ){
    var self = this;
  //  fn();
    // Evitar doble conexion
    if( self.__isConnected ) return fn(null);

    sql.connect(cfg, function( err ){
      self.__isConnected = true;
      fn(err);
    });
  },

  tree : function( callback ) {
    var self = this;
    this.__runSql( function( err ) {
        var q = 'SELECT RTRIM(LTRIM(RUBRO)) id, RTRIM(LTRIM(NOMBRE)) nombre FROM RUBROS'
            + ' WHERE 0 < ( SELECT COUNT(*) FROM Productos WHERE Productos.RUBRO = Rubros.RUBRO ) '
            ;
      var i = 0;

        var request = new sql.Request();
        request.query(q)
            .then( function( data ){
              data.forEach(function(row, idx ){
                //request.input('rubro_' + idx, sql.VarChar, row.id )
                var gq = 'SELECT RTRIM(LTRIM(GRUPO)) id, RTRIM(LTRIM(NOMBRE)) nombre FROM Grupos '
                       + " WHERE RUBRO = '" + row.id + "' ";


                new sql.Request().query(gq)
                  .then( function( grupos ){
                    data[idx].grupos = grupos;
                    i++;

                    if( i == data.length ) {
                      callback(null, data);
                    }
                  }).catch( function(error){
                    callback(error, null);
                  })
              })
            })
            .catch( function(error){
                console.log(error);
                callback(error, null);
                //return debugSqlError(error, q, callaback)
            })
    });
  }

}

module.exports = Model;
