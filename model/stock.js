var
       cfg  = require('../cfg/sql'),
       sql  = require('mssql');

var _ = require('lodash');
var multiline = require('multiline');
var Mustache = require('mustache')
var conn = require('../lib/connection');

conn.connect();



var modelo = {
    find: function( type, str, cuenta, callback ) {
        if( type == 'codProducto')
            return modelo.findByCodigo( str, callback);
        return modelo.findByCodigoCliente(cuenta, str, callback);
    },

    findByCodigo : function( str, callback ) {
        if( conn._connected ) {
            var request = new conn.Driver.Request(conn._instance);
            var productVars = {
                conds : []
            };

            request.input('criteria', conn.Driver.VarChar, '%' + str + '%');


            str.split(' ').forEach(function(str, idx){
                productVars.conds.push({
                    str : 'subproducto_' + idx,
                    idx : idx,
                    val : str
                });

                request.input('subproducto_' + idx, conn.Driver.VarChar, '%' + str + '%');
            })


            var q = multiline(function(){/*
                                SELECT
                                    Productos.CODPRODUCTO,
                                    Productos.NOMBRE,
                                    stockactual1 =
                                        CASE WHEN Stock.STOCKACTUAL IS NULL
                                            THEN 0
                                            ELSE stock.stockactual
                                        END,
                                    stockminimo =
                                        CASE WHEN Stock.STOCKMINIMO IS NULL
                                            THEN 0 ELSE stock.stockminimo
                                        END
                                FROM
                                    Productos
                                LEFT JOIN
                                    Stock ON Stock.CODPRODUCTO = Productos.CODPRODUCTO AND {{ deposito }} = Stock.DEPOSITO
                                WHERE
                                    Productos.TabaOnLine = 1 AND Productos.TABAONLINEFECHAPUBSQL <= GETDATE() AND Productos.Perfil <= 2

                                    {{# conds }}

                                        and
                                    (
                                        Productos.codproducto like  @{{ str }}
                                            OR
                                        Productos.nombre like @{{ str }}
                                            OR
                                        Productos.sinonimo like @{{ str }}
                                    )

                                    {{/ conds }}
                    */});

            var renderVars = _.extend(TabaApp.empresa, productVars);
            var q = Mustache.render(q, renderVars);

            request
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function (error) {
                    callback( error, null );
                })

        } else {
            callback({ error : ' no se puede conectar '}, null);
        } // end if conn._connected
    }, // end fn findByCodigo

    findByCodigoCliente : function ( user, str, callback ) {
        if( conn._connected ) {
            var request = new conn.Driver.Request(conn._instance);
            var productVars = {
                conds : []
            };

            request.input('criteria', conn.Driver.VarChar, '%' + str + '%');
            request.input('cuenta', conn.Driver.VarChar, user.Cuenta );


            str.split(' ').forEach(function(str, idx){
                productVars.conds.push({
                    str : 'subproducto_' + idx,
                    idx : idx,
                    val : str
                });

                request.input('subproducto_' + idx, conn.Driver.VarChar, '%' + str + '%');
            });

            var q = multiline(function(){/*
                     SELECT
                        Productos.CODPRODUCTO,
                        Productos.NOMBRE,
                        stockactual1=CASE WHEN Stock.STOCKACTUAL IS NULL THEN 0 ELSE stock.stockactual END,
                        stockminimo = CASE WHEN Stock.STOCKMINIMO IS NULL THEN 0 ELSE stock.stockminimo END
                    FROM
                        Productos
                    LEFT JOIN
                        Stock ON Stock.CODPRODUCTO = Productos.CODPRODUCTO AND {{ deposito }} = Stock.DEPOSITO
                    INNER JOIN
                        ProductosClientes on
                            (
                                (
                                    ProductosClientes.codproducto=Productos.codproducto
                                        and
                                    ProductosClientes.cuenta=@cuenta
                                )
                            )
                    WHERE

                      Productos.TabaOnLine = 1 AND Productos.TABAONLINEFECHAPUBSQL <= GETDATE() AND Productos.Perfil <= 2

                        {{# conds }}

                            and
                            (
                                ProductosClientes.CODPRODUCTOCLIENTE like  @{{ str }}
                                    OR
                                ProductosClientes.Nombre like @{{ str }}
                            )

                        {{/ conds }}

                    */});

            var renderVars = _.extend(TabaApp.empresa, productVars);
            var q = Mustache.render(q, renderVars );

        
            request
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function (error) {
                    callback( error, null );
                })

        } else {
            callback({ error : ' no se puede conectar '}, null);
        } // end if conn._connected
    } // end fn findByCodigoCliente
}

module.exports = modelo;
