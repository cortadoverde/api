var
       cfg  = require('../cfg/sql'),
       sql  = require('mssql'),
       conn = require('../lib/connection');

console.log(__filename, __dirname);

conn.connect();


// Utiles
var _ = require('lodash');
var Mustache = require('mustache');
var multiline = require('multiline');



var usuario = {

    login : function( type, data, callback ) {

        if( type == 'cliente' ) {
            usuario.login_cliente(data, callback);
        } else {
            usuario.login_vendedor(data, callback);
        }
    },

    login_cliente : function( data, callback ) {
        sql.connect(cfg, function(err){
            var request = new sql.Request();

            request.input('cuenta', sql.VarChar, data.cuenta );
            request.input('clave', sql.VarChar, data.clave );

            request.query([
                ' SELECT Cuenta, LTRIM(RTRIM(WebClave)) WebClave, LTRIM(RTRIM(WebClavePedido)) WebClavePedido, LTRIM(RTRIM(WebClaveDiponibilidad)) WebClaveDiponibilidad, ',
                ' LTRIM(RTRIM(Nombre)) Nombre, LTRIM(RTRIM(RazonSocial)) RazonSocial ',
                ' FROM Clientes WHERE CUENTA = @cuenta ',
                ' AND (WEBCLAVE = @clave or WEBCLAVEPEDIDO = @clave or WEBCLAVEDIPONIBILIDAD = @clave) '
            ].join(''), function( err, recordset ){
                callback(err, recordset)
            })
        });
    },

    login_vendedor : function( data, callback ) {
        sql.connect(cfg, function(err){
            var request = new sql.Request();

            request.input('cuenta', sql.VarChar, data.cuenta );
            request.input('clave', sql.VarChar, data.clave );

            request.query([
                'SELECT Vendedor, WebClave ',
                'FROM Vendedores WHERE VENDEDOR = @cuenta ',
                'AND WEBCLAVE = @clave'
            ].join(''), function( err, recordset ){
                callback(err, recordset)
            })
        });
    },

    // busqueda de clientes
    find : function( user, str, callback ) {

        if( conn._connected ) {
            var request = new conn.Driver.Request(conn._instance);
            var vendedor = user._id;

            request.input('str', conn.Driver.VarChar, '%' + str + '%');

            var q = multiline(function(){/*
                SELECT
                    ROW_NUMBER() OVER(ORDER BY Clientes.Cuenta DESC) AS idx,
                    RTRIM(Clientes.CUENTA) Cuenta,
                    LTRIM(RTRIM(Clientes.Nombre)) Nombre,
                    LTRIM(RTRIM(Clientes.RAZONSOCIAL)) AS RazonSocial,
                    LTRIM(RTRIM(Clientes.DOMICILIO)) AS Domicilio,
                    LTRIM(RTRIM(Clientes.CODIGOPOSTAL)) AS CodigoPostal,
                    LTRIM(RTRIM(Ciudades.NOMBRE)) AS Ciudad
                FROM
                    ClientesVendedorWeb
                INNER JOIN
                    Clientes ON ClientesVendedorWeb.CUENTA = Clientes.CUENTA
                INNER JOIN
                    Ciudades ON Clientes.CIUDAD = Ciudades.CIUDAD
                WHERE
                    (
                        (
                            ClientesVendedorWeb.VENDEDOR = {{ vendedor }} or {{ vendedor }} = '99'
                        )
                            AND
                        (
                            clientes.cuenta like @str OR clientes.nombre like @str
                        )
                    )
                UNION
                    SELECT
                        ROW_NUMBER() OVER(ORDER BY INNERR.CUENTA DESC) AS idx,
                        INNERR.CUENTA AS cuenta,
                        INNERR.NOMBRE AS nombre,
                        INNERR.RAZONSOCIAL AS razonsocial,
                        INNERR.DOMICILIO AS domicilio,
                        INNERR.CODIGOPOSTAL AS codigopostal,
                        Ciudades_1.NOMBRE AS ciudad
                    FROM
                        Clientes AS INNERR
                    INNER JOIN
                        Ciudades AS Ciudades_1 ON INNERR.CIUDAD = Ciudades_1.CIUDAD
                    WHERE
                        (
                            (
                                INNERR.VENDEDOR = {{ vendedor }} OR {{ vendedor }} = '99'
                            ) AND
                            (
                                INNERR.cuenta like @str OR INNERR.nombre like @str
                            )
                        )
                    ORDER BY nombre
            */});

            var q = Mustache.render(q, { vendedor : vendedor });

            request
                    .query(q)
                    .then( function (recordset) {
                       return callback( null, recordset );
                    })
                    .catch( function (error) {
                       return callback( error, null );
                    })
        } else {
            return callback({ error : ' no se puede conectar '}, null);
        }

    },

    findByCuenta : function( cta, callback ) {
        if( conn._connected ) {
            var request = new conn.Driver.Request(conn._instance);

            request.input('cuenta', conn.Driver.VarChar, cta);

            request.query(
                [
                    'SELECT RTRIM(Cuenta) Cuenta, LTRIM(RTRIM(Nombre)) Nombre, LTRIM(RTRIM(RazonSocial)) RazonSocial ',
                    'FROM Clientes WHERE CUENTA = @cuenta '
                ].join('')
            )
            .then(
                function (recordset) {
                    callback(null, recordset)
                }
            )
            .catch(
                function (e) {
                    callback(e, null);
                }
            );

        } else {
            callback({ error : ' no se puede conectar '}, null);
        }
    }
}


module.exports = usuario;
