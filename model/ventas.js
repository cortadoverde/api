var
       cfg  = require('../cfg/sql'),
       sql  = require('mssql');
cfg.pool = {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000
};

var modelo = {

    // Organizar en donde va a quedar mejor
    get_rubros : function( callback ) {
        sql.connect(cfg, function(err){

          var q = 'SELECT RTRIM(LTRIM(RUBRO)) id, RTRIM(LTRIM(NOMBRE)) nombre FROM Rubros'
                + ' WHERE 0 < ( SELECT COUNT(*) FROM Productos WHERE Productos.RUBRO = Rubros.RUBRO ) '
                ;

          new sql.Request()
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function(error){
                    return debugSqlError(error, q, callaback)
                })
        });
    },

    get_vendedores : function( callback ) {
        sql.connect(cfg, function(err){

          var q = 'SELECT RTRIM(LTRIM(VENDEDOR)) id, RTRIM(LTRIM(NOMBRE)) nombre FROM VENDEDORES '
              //  + ' WHERE 0 < ( SELECT COUNT(*) FROM Productos WHERE Productos.RUBRO = Rubros.RUBRO ) '
                ;

          new sql.Request()
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function(error){
                    return debugSqlError(error, q, callaback)
                })
        });
    },

    get_marcas : function( callback ) {
        sql.connect(cfg, function(err){

          var q = 'SELECT RTRIM(LTRIM(NUMERO)) id, RTRIM(LTRIM(NOMBRE)) nombre FROM MARCAS '
              //  + ' WHERE 0 < ( SELECT COUNT(*) FROM Productos WHERE Productos.RUBRO = Rubros.RUBRO ) '
                ;

          new sql.Request()
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function(error){
                    return debugSqlError(error, q, callaback)
                })
        });
    },

    dia : function( params, callback ) {
        // Fix si no viene con parametros adicionales
        if( typeof params == 'function' ) {
          callback = params;
          params = {};
        }

        sql.connect(cfg, function(err){
            var request = new sql.Request();

            if( params.desde && params.desde.date ){
              request.input('desde', sql.DateTime, params.desde.date );
              request.input('hasta', sql.DateTime, params.hasta.date );
            }

            // == SECCION: FIELDS ==
            // Si no hay parametros adicionales solo buscara entre un rango de
            // fechas determinado
            var q = 'SELECT '
                  + ' DAY(F.FECHASQL) as dia, '
                  + ' MONTH(F.FECHASQL) as mes,'
                  + ' YEAR(F.FECHASQL) as anio, ';

            // TOTAL SEGUN CONCEPTO DE COMPROBANTE
            q += commonQuery( params );
            // Filtro por fechas desde hasta
            q += ' AND ( FM.FECHASQL >= @desde AND  FM.FECHASQL <= @hasta ) ';
            q += applyFilters( request, params );
            // == SECCION: AGRUPACION Y ORDEN ==
            q += ' GROUP BY YEAR(F.FECHASQL), MONTH(F.FECHASQL),DAY(F.FECHASQL) '
              +  ' ORDER BY YEAR(F.FECHASQL), MONTH(F.FECHASQL),DAY(F.FECHASQL) '
            ;

          //  if( params.debug )
          console.log(q);
          console.log(params);

            request
                .query(q)
                .then( function (recordset) {
                    callback( null, recordset );
                })
                .catch( function(error){
                    return debugSqlError(error, q, callaback)
                })
        })
    },

    mes : function( params, callback ) {
      sql.connect(cfg, function(err){

          var request = new sql.Request();
          if( params.desde && params.desde.date ){
            request.input('desde', sql.DateTime, params.desde.date );
            request.input('hasta', sql.DateTime, params.hasta.date );
          }
          // == SECCION: FIELDS ==
          // Si no hay parametros adicionales solo buscara entre un rango de
          // fechas determinado
          var q = 'SELECT '
                + ' 1 as dia, '
                + ' MONTH(F.FECHASQL) as mes, '
                + ' YEAR(F.FECHASQL) as anio, ';

          // TOTAL SEGUN CONCEPTO DE COMPROBANTE
          q += commonQuery( params );
          // Filtro por fechas desde hasta
          q += ' AND ( FM.FECHASQL >= DATEADD(month, DATEDIFF(month, 0, @desde), 0) AND  FM.FECHASQL <= DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, @hasta) + 1, 0))  ) ';
          q += applyFilters( request, params );
          // == SECCION: AGRUPACION Y ORDEN ==
          q += ' GROUP BY YEAR(F.FECHASQL), MONTH(F.FECHASQL) '
            +  ' ORDER BY YEAR(F.FECHASQL), MONTH(F.FECHASQL) '
          ;

          console.log(q);
          console.log(params);

          request
              .query(q)
              .then( function (recordset) {
                  callback( null, recordset );
              })
              .catch( function(error){
                  return debugSqlError(error, q, callaback)
              })
      })
    },

    semana: function( params, callback ) {
      sql.connect(cfg, function(err){
          var request = new sql.Request();
          request.input('desde', sql.DateTime, params.desde.date );
          //request.input('hasta', sql.DateTime, params.hasta.date );

          // == SECCION: FIELDS ==
          // Si no hay parametros adicionales solo buscara entre un rango de
          // fechas determinado
          var q = 'SELECT '
                + ' DAY(F.FECHASQL) as dia, '
                + ' MONTH(F.FECHASQL) as mes,'
                + ' YEAR(F.FECHASQL) as anio, ';

          // TOTAL SEGUN CONCEPTO DE COMPROBANTE
          q += commonQuery( params );
          // Filtro por fechas desde hasta
          //q += ' AND ( FM.FECHASQL >= @desde AND  FM.FECHASQL <= @hasta ) ';
          q += ' AND FM.FECHASQL >= DATEADD(dd, -(DATEPART(dw, @desde)-1), @desde) AND FM.FECHASQL <= DATEADD(dd, 7-(DATEPART(dw, @desde)), @desde) '

          q += applyFilters( request, params );
          // == SECCION: AGRUPACION Y ORDEN ==
          q += ' GROUP BY YEAR(F.FECHASQL), MONTH(F.FECHASQL), DAY(F.FECHASQL) '
            +  ' ORDER BY YEAR(F.FECHASQL), MONTH(F.FECHASQL), DAY(F.FECHASQL) '
          ;

          console.log(q);
          console.log(params);

          request
              .query(q)
              .then( function (recordset) {
                  callback( null, recordset );
              })
              .catch( function(error){
                  return debugSqlError(error, q, callaback)
              })
      })
    }

}

function debugSqlError( error, q, callback ) {
  console.log(error);
  console.log(q);
  return callback(error, null);
}

function commonQuery( params )
{
  var q = '';

  // TOTAL SEGUN CONCEPTO DE COMPROBANTE
    q += ' SUM(CASE TC.CONCEPTO '
      +  " WHEN 'D' THEN FM.IMPORTENETO * 1 "
      +  " WHEN 'C' THEN FM.IMPORTENETO * -1 "
      +  " ELSE FM.IMPORTENETO * 0 "
      +  ' END ) as total '
    ;
  // == SECCION: TABLAS ==
      // V010 -> Facturas
    q += ' FROM Facturas F '
      // V011 -> FacturasMov
      +  ' INNER JOIN FacturasMov FM ON F.TIPOID = FM.TIPOID AND F.NUMEROID = FM.NUMEROID '
      // V001 -> TablaDeComprobantes
      +  ' LEFT OUTER JOIN TablaDeComprobantes TC  ON F.TIPOID = TC.TIPO '
      +  ' LEFT OUTER JOIN Productos P ON P.CODPRODUCTO = FM.CODPRODUCTO '
      ;
  // == SECCION: CONDICIONES ==
    q += ' WHERE  TC.RANKING = 1 AND TC.VENTAS = 1 AND NOT FM.ANULADO > 0 '
      ;

  return q;
}

function applyFilters(request, params ) {

  var q = ' ';

  // Cambiar la consulta si tiene como parametro el vendedor
  if( params.vendedores  ) {
      q += ' AND ( ';
      var i = 0;
      params.vendedores.forEach(function(data, idx ){
        request.input('vid_' + idx , sql.VarChar, data );
        if( i > 0 ) q += ' OR '
        q += ' F.VENDEDOR = @vid_' + idx + ' ';
        i++
      })
      q += ' ) '
  }

  if( params.rubros ) {
    q += ' AND ( ';
    var i = 0;
    params.rubros.forEach(function(data, idx ){
      request.input('ruid_' + idx , sql.VarChar, data );
      if( i > 0 ) q += ' OR '
      q += ' P.RUBRO = @ruid_' + idx + ' ';
      i++;

      if( params.grupos ) {
        params.grupos.forEach(function(grupo, idx ){
          console.log(arguments);
          if( grupo.rubro == data ) {
            q += ' AND ( ';
            var j = 0;
            grupo.grupos.forEach(function( grupoid, gidx ){
              request.input('guid_' + gidx, sql.VarChar, grupoid );
              if( j > 0 ) q += ' OR ';
              q += ' P.GRUPO = @guid_' + gidx + ' ';
              j++;
            })
            q += ' ) ';
          }
        })
      }

    })
    q += ' ) '


  }

  /*
  if( params.grupos ) {
    params.grupos.forEach(function(data, idx ){
      request.input('grid_' + idx , sql.VarChar, data );
      q += ' AND P.GRUPO = @grid_' + idx + ' ';
    })
  }
  */
  if( params.marcas ) {
    params.marcas.forEach(function(data, idx ){
      request.input('maid_' + idx , sql.VarChar, data );
      q += ' AND P.MARCA = @maid_' + idx + ' ';
    })
  }

  return q;

}

module.exports = modelo;
