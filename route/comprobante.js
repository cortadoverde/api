var model = require('../model/comprobante');

exports.ver = function(req, res) {

    var tipoId = req.params.tipoId || false;
    var numeroId = req.params.numeroId || false;

    var callback = function(err, data){
        if( err ) {
            return res.status(404).send({error: err})
        }
        return res.send(data)
    }

    return model.getComprobante( tipoId, numeroId , callback );

}
