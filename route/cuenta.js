var model = require('../model/cuenta');

exports.resumen = function(req, res) {
    var type = req.query.type || 'pending';
    var user = req._user._current || false;
    var callback = function(err, data){
        if( err ) {
            return res.status(404).send({error: err})
        }            
        return res.send(data)
    }

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }

    if( type == 'pending' )
        return model.getPending( user.Cuenta , callback );
    return model.getResumen( user.Cuenta , callback );
    
}