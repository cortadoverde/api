var mongoose = require('mongoose');
var m        = require('../model/descargas');
var fs       = require('fs');
var _        = require('lodash');
var move     = require('../lib/move');
var path     = require('path');

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

mongoose.connect('mongodb://127.0.0.1:27017/filemanager');
/*
exports.index = function(req, res){

    res.setHeader('Content-Type', 'application/json');



    m.Group.findOne({}).populate('file').exec(function(e, doc){
      res.send(doc);
    })


}
*/
exports.index = function( req, res ) {
  m.Group.find().populate('files').exec(function(e, doc){
    console.log(typeof doc, doc.length);
    res.send(doc);
  })

  return;

}


exports.addGrupo = function( req, res ) {
  var groupData = req.body.group;
  var record = new m.Group(groupData);
  record.save(function(err, data){
    if( err ) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.send(data);
  })

}

exports.addFile = function( req, res ) {
  var fileData = req.body.file;
  var record = new m.File(fileData);
  record.save(function(err, data){
    if( err ) throw err;
    res.setHeader('Content-Type', 'application/json');
    res.send(data);
  })
}

exports.downloadFile = function ( req, res ) {
  var file_id = req.params.file_id;
  var object_id = new mongoose.Types.ObjectId(file_id);
  m.File.findOne({ _id : object_id }, function( e, fileObject ) {
    if( fileObject === null ) {
      return res.send('No se encontro ningun archivo');
    }

    if( e ) {
      // buscar si esta asignado a un grupo y borrarlo
      throw e;
    }

    res.download( TabaApp._root + fileObject.path );

  });

}

exports.deleteGroup = function( req, res ) {

  var grupo_id  = req.params.group_id;
  var object_id = new mongoose.Types.ObjectId( grupo_id );

  m.Group.findOne({ _id : object_id }, function( err, fileObject ) {
    fileObject.remove();
  });

  res.send('ok');

}

exports.deleteFile = function( req, res ) {
  var file_id = req.params.file_id;
  var object_id = new mongoose.Types.ObjectId(file_id);
  m.File.findOne({ _id : object_id }, function( e, fileObject ) {
    if( fileObject === null ) {
      return res.send('No se encontro ningun archivo');
    }

    if( e ) {
      // buscar si esta asignado a un grupo y borrarlo
      throw e;
    }

    fileObject.remove(function(e, oldData){
      if( e ) throw e;

      var p = path.dirname( path.dirname( __dirname ) );


      fs.unlink(  p + '/tabaweb' + oldData.path, function( err ){
        if( err ) {
          console.log(err);
          return res.send('no se puede eliminar el archivo fisico!');
        }

        m.Group.findOne( { files : { $in : [object_id] } }, function( eg, groupObject ) {

          var assignedFiles = groupObject.files;
          var newSchema = _.without(assignedFiles, object_id); //_.difference
          m.Group.update( { _id : groupObject._id }, { $set : { files : newSchema } }, function( e ){
            groupObject

            m.Group.findOne(
              {
                "_id" : new mongoose.Types.ObjectId(groupObject._id)
              }
            )
            .populate('files')
            .exec(function(e, doc){
              return res.send(doc); // Agregar el upload
            });

          } );

        });

      });

    });

  });

}

exports.upload = function ( req, res ) {
    var file     = req.file;
    console.log( file );
    var tmp      = file.path;
    var stats    = fs.statSync(tmp);
    var realFile = guid() + '_' +  file.name;
    var newFile  = TabaApp._root + '/../tabaweb/descargas/' + realFile ;



    move.move( tmp, newFile, function( err ) {
      if( ! err ) {

        var docFile = new m.File({
          name : file.name,
          path : '/descargas/' + realFile,
          size : stats['size'],
          created : stats['ctime']
        });

        docFile.save(function(e, nFile){

          if( req.body && req.body.group_id ) {
            m.Group.findOne(
              {
                "_id" : new mongoose.Types.ObjectId(req.body.group_id)
              }
            )
            .populate('files')
            .exec(function(e, doc){
              doc.files.push(nFile);
              doc.save();

              return res.send(doc); // Agregar el upload
            });
          } else {
            return res.send(nFile);
          }

        });

      }
    });
}
