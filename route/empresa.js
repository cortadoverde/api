var model = require('../model/empresa');

exports.index = function(req, res){
    res.setHeader('Content-Type', 'application/json');
    model.get(function(err, data){
        if( err ) {
            res.send({ error: 'no se puede ejecutar la consulta' })
        }else {
            res.send(data);
        }
    })
}