/**
 * Sub App Express
 * base => /nvta/
 */

// Dependencias
var model   = require('../model/notadeventa');
var express = require('express');
var app     = express();

// Index
app.get('/', function( req, res ) {
  res.setHeader('Content-Type', 'application/json');
    model.a(function( error, response ){
      if( error ) throw error;
      console.log(response);
      res.send( JSON.stringify(response) );
    })
})

app.post('/', function( req, res ) {
  res.setHeader('Content-Type', 'application/json');

  res.send( JSON.stringify( req.body ) );

})

module.exports = app;
