var model = require('../model/pedidos');

exports.misPedidos = function(req, res) {

    var user = req._user._current || false;

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }

    model.getLastOrderByCuenta( user.Cuenta, function ( error, data ) {
        if( error )
            return res.status(404).send({error: error});

        return res.send(data);
    });

}

exports.pendings = function( req, res ) {
    var user = req._user._current || false;

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }

    model.getPendingByCuenta( user.Cuenta, function( error, data ){
        if( error )
            return res.status(404).send({error: error})
        return res.send(data)
    })
}

exports.deleteAllPending = function( req, res ) {
   var user = req._user._current || false;

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }

    model.deleteAllPendingsByCuenta( user.Cuenta, function( error, data ){
        if( error )
            return res.status(404).send({error: error})
        return res.send(data)
    })
}

exports.deletePending = function( req, res ) {
    var registro = req.params.id;
    var user = req._user._current || false;

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }

    model.deletePending( registro, user.Cuenta, function( error, data ){
        if( error )
            return res.status(404).send({error: error})
        return res.send({ok : 'Registro eliminado'})
    })
}

exports.confirm = function( req, res ) {
    var user = req._user._current || false;

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }

    model.confirm( user.Cuenta, function( error, data ){
        if( error )
            return res.status(404).send({error: error})
        return res.send({ok : 'Pedido confirmado'})
    })

}

exports.create = function( req, res ) {
    var data = req.body.productos;
    var user = req._user._current || false;

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }
    model.create( user.Cuenta, data, function(error, data){
        if( error )
            return res.status(404).send({error: error})

        if( data.length == 0 )
          return res.send({ok : 'Pedido realizado'})

        return res.send({error: data});
    })
}

exports.find = function(req, res) {
    var str = req.query.cuenta || '';
        str = decodeURIComponent(str);

    if( str.length > 2 ) {

        model.find(str, function(err, data){
            if( err ) {
                return res.status(404).send({error: err})
            }

            res.send(data)
        })
    } else {
        res.send(req.body)
    }
}
