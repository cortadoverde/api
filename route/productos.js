var model  = require('../model/productos');

exports.list = function( req, res ) {
    model.listAll(function(err, data){
        if( err ) {
            return res.status(404).send({error: err});
        }
        return res.send(data)        
    })
   
}

exports.buscar = function( req, res ) {
    if( ! req.query.codProducto && ! req.query.codCliente ) {
        return res.status(404).send({error: 'No se definio el tipo de busqueda'})
    }

    var user = req._user._current || false;

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }


    search_type = ! req.query.codCliente ? 'codProducto' : 'codCliente';
    str = ! req.query.codProducto ? req.query.codCliente : req.query.codProducto ;

    model.find( search_type, str, user.Cuenta,  function( err, data  ) {

        if( err ) {
            return res.status(404).send({error: err});
        }
        return res.send(data)  
    })
}

exports.createCodigo = function ( req, res ) {
    var data = req.body.codigos;
    var user = req._user._current || false;

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }    
    model.createCodigoCliente( user.Cuenta, data, function(error, data){
        if( error ) 
            return res.status(404).send({error: error})
        return res.send({ok : 'Producto agregado'})
    })
    
}