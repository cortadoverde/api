var model = require('../model/stock');

exports.find = function(req, res) {
    var str = req.query.str || '';
    var type = req.query.type || '';

    str = decodeURIComponent(str);

    var user = req._user._current || false;

    if( ! user || ! user.Cuenta ) {
        return res.status(404).send({error: 'no se definio un usuario'})
    }


    model.find(type, str, user, function(err, data){
        if( err ) {
            return res.status(404).send({error: err})
        }

        return res.send(data)
    })

}
