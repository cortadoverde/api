var express = require('express'),
    router  = express.Router();

router.get('/', function( req, res ){
  var list = {
    '/ventas' : {
      '/dia' : { descripcion : 'Obtiene las ventas por dia segun un rango de fecha'}
    }
  }
  res.send(list);
});

router.use( '/ventas' , require('./ventas') );
router.use( '/clientes' , require('./clientes') );
router.use( '/rubros' , require('./rubros') );

module.exports = router;
