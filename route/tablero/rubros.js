var express = require('express');
var router = express.Router();
var model  = require('../../model/rubros');

router.get('/', function(req, res) {
  var rubrosModel = new model();
  rubrosModel.tree(function(error, data){
    res.send(data);
  });

});

module.exports = router;
