var     jwt  = require('jsonwebtoken'),
      model  = require('../model/usuario'),
        auth = require('../lib/auth')
;

exports.login = function( req, res ) {
    res.setHeader('Content-Type', 'application/json');

    var cuenta = req.body.cuenta || '',
         clave = req.body.clave  || '';

    if( cuenta == '' || clave == '' ) {
        return res.status(401).send({ error: 'el campo usuario y contraseña son obligatorios' } );
    }

    // Por defecto se trata de loguear como cliente
    var type = 'cliente';

    if( cuenta.length <= 3 ) {
        // esta definido y es cliente o vendedor
        type = 'vendedor';
    }

    model.login( type, req.body, function( err, data ){
        if( err ) {

            return res.status(401).send({
                error: 'problema con la conexion verifique que este conectado a un servidor activo',
                level: 1
            });
        }

        if( data.length < 1 ) {
            return res.status(401).send({
              error: 'usuario o contraseña incorrecta',
              level: 512
            });
        }

        var     AuthCode = 0,
             currentUser = data[0],
                      id;

        console.log('route\\usuario:', currentUser )

        if( type == 'cliente' ) {

            AuthCode =
                currentUser.WebClave == clave ? 3 : (
                    currentUser.WebClavePedido == clave ? 2 : (
                        currentUser.WebClaveDiponibilidad == clave  ? 1 : 0
                    )
                );
            id = currentUser.Cuenta;

            delete currentUser.WebClaveDiponibilidad;
            delete currentUser.WebClavePedido;
            delete currentUser.WebClave;

        } else {
            AuthCode = 3;
            id = currentUser.Vendedor;
            delete currentUser.WebClave;
        }

        currentUser._id = id;
        currentUser._perms = AuthCode;

        auth.createAndStoreToken(currentUser, 60*60, function(err, token) {
            if (err) {
                return res.send(400);
            }

            //Send back token
            return res.status(200).send({token: token});
        });

    })
}

exports.logout = function( req, res ) {
    auth.expireToken(req.headers, function(err, success) {
        if (err) {

            return res.send(401);
        }

        if (success) res.send(200);
        else res.send(401);
    });
}

exports.me = function(req, res) {
    return res.send(req._user);
}

exports.find = function(req, res) {
    var str = req.query.cuenta || '';
        str = decodeURIComponent(str);

    if( str.length > 2 ) {

        model.find(req._user, str, function(err, data){
            if( err ) {
                return res.status(404).send({error: err})
            }

            res.send(data)
        })
    } else {
        res.send(req.body)
    }
}

exports.setCuenta = function( req, res ) {
    var cta = req.params.cuenta;
    var currentUser = req._user;
    currentUser._current = cta;
    model.findByCuenta(cta, function(err, new_user){
        if( err )
            return res.status(404).send(err);

        currentUser._current = new_user[0];
        auth.redisHelper.setTokenWithData(currentUser._token, currentUser, TabaApp.ttl, function(etoken, success){
            return res.status(200).send(currentUser._current);
        });

    })


}
