var express = require('express');
var router = express.Router();
var model  = require('../model/ventas');

function _parseDate( dateParam, hasta ) {
  var _clone = String(dateParam);
  var result = _clone.trim();
  var time = {
    yy : 1900,
    mm : 1,
    dd : 1,
    h : 0,
    m : 0,
    s : 0,
    ms: 0
  };

  if( ! hasta ){
    if( result.indexOf(':') !== -1 ) {
      reg_time = result.match(/(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/);
      if( reg_time ) {
        time.h = reg_time[1]; time.m = reg_time[2]; time.s = reg_time[3];
      }
    }
  } else {
    time.h = 23;
    time.m = 59;
    time.s = 59;
  }

  if( result.indexOf('-') !== -1 ) {
    // formatos posibles
    // YYYY-MM-DD
    // DD-MM-YYYY
    var reg_yyyymmdd = result.match( /^(\d{4})-(\d{1,2})-(\d{1,2})/ );
    if( reg_yyyymmdd ) {
      time.yy = reg_yyyymmdd[1];
      time.mm = reg_yyyymmdd[2] - 1;
      time.dd = reg_yyyymmdd[3];
    }

    var reg_ddmmyyyy = result.match( /^(\d{1,2})-(\d{1,2})-(\d{4})/ );
    if( reg_ddmmyyyy ) {
      time.yy = reg_ddmmyyyy[3];
      time.mm = reg_ddmmyyyy[2] - 1;
      time.dd = reg_ddmmyyyy[1];
    }
  }

  if( result.indexOf('/') !== -1 ) {

    // formatos posibles
    // YYYY/MM/DD
    // DD/MM/YYYY
    var reg_yyyymmdd = result.match( /^(\d{4})\/(\d{1,2})\/(\d{1,2})/ );

    if( reg_yyyymmdd ) {
      time.yy = reg_yyyymmdd[1];
      time.mm = reg_yyyymmdd[2] - 1;
      time.dd = reg_yyyymmdd[3];
    }

    var reg_ddmmyyyy = result.match( /^(\d{1,2})\/(\d{1,2})\/(\d{4})/ );
    if( reg_ddmmyyyy ) {
      time.dd = reg_ddmmyyyy[1];
      time.mm = reg_ddmmyyyy[2] - 1;
      time.yy = reg_ddmmyyyy[3];
    }
  }

  if( result.indexOf('//') === -1 && result.indexOf('-') === -1 ) {
    var checkYear = result.slice(0,4);
    if( checkYear > 31 ) { // No es ni mes ni dia
        var reg = result.match(/^(\d{4})(\d{1,2})(\d{1,2})/);
        if( reg ) {
          time.dd = reg[3];
          time.mm = reg[2] - 1;
          time.yy = reg[1];
        }
    }
  }

  var ob_return = new Date( time.yy, time.mm, time.dd, time.h, time.m, time.s );
  var ret =  { time: time , date : ob_return };

  return ret;
}

function parseFilters( query ) {
  var params = {};

  if ( ! query ) return params;

  if( query.vendedores ) {
    params.vendedores = query.vendedores
  }

  if( query.rubros ) {
    params.rubros = query.rubros;
  }

  if( query.marcas ) {
    params.marcas = query.marcas;
  }

  if( query.desde ) {
    params.desde = _parseDate(query.desde);
  }

  if( query.hasta ) {
    params.hasta = _parseDate(query.hasta, true);
  }

  return params;
}



// define the home page route
router.get('/', function(req, res) {
  var routes = {
    'get /' : {
        description : 'Listado de metodos disponibles'
    },

    'get /dia' : {
        description : 'Obtiene las ventas por dia',
        parametros : {
          'desde *' : 'YYYY-mm-dd (ej: 2016-02-01)',
          'hasta *' : 'YYYY-mm-dd (ej: 2016-02-29)'
        }
    },

    '@todo ' : 'terminar este indice :p '
  };

  res.send(routes);
});

router.get('/dia', function( req, res ){
  var params = parseFilters(req.query);

  model.dia(params, function(error, data){
    res.send(data);
  })
});

router.get('/mes', function( req, res ) {
  var params = parseFilters(req.query);
  model.mes( params, function( error, data ){
    res.send(data);
  })
})

router.get('/semana', function( req, res ) {
  var params = parseFilters(req.query);
  model.semana( params, function( error, data ){
    res.send(data);
  })
});

router.get('/rubros', function( req, res ){
  model.get_rubros(function( error, data ){
    res.send( data );
  })
})

router.get('/vendedores', function( req, res ){
  model.get_vendedores(function( error, data ){
    res.send( data );
  })
});

router.get('/marcas', function( req, res ){
  model.get_marcas(function( error, data ){
    res.send( data );
  })
});

module.exports = router;
